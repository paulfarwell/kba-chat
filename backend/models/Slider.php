<?php
namespace backend\models;

use Yii;
use common\models\Chat;

class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'top_sliders';
    }

    public function rules()
    {
        return [
            
            [['slider_1', 'slider_2', 'slider_3','slider_4','slider_5','slider_6'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
           'slider_1' => 'Slider 1',
           'slider_2' => 'Slider 2',
           'slider_3' => 'Slider 3',
           'slider_4' => 'Slider 4',
           'slider_5' => 'Slider 5',
           'slider_6' => 'Slider 6',
        ];
    }
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return [
    //        [['Chat','AnswersID','Question_1', 'Question_2'], 'required'],
    //         [['Question_1', 'Question_2','Question_3','Question_4','Question_5'], 'string', 'max' => 255]


    //     ];
    // }



}
