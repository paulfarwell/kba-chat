<?php
namespace backend\models;
 
use Yii;
use common\models\Chat;
use common\models\Reviews;
use backend\models\ReviewAnswers;
 
class ReviewQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review_questions';
    }
     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['Chat','Question_1','Answer_set1', 'Question_2','Answer_set2'], 'required'],
           [['Question_1', 'Question_2','Question_3','Question_4','Question_5'], 'string', 'max' => 255],
           [['Answer_set1', 'Answer_set2','Answer_set3','Answer_set4','Answer_set5'], 'integer']
            
          
        ];
    }

    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['ID' => 'Chat']);
    }
 

     public function getReviewanswers1()
    {
        return $this->hasOne(ReviewAnswers::className(), ['id' => 'Answer_set1']);
    }
     public function getReviewanswers2()
    {
        return $this->hasOne(ReviewAnswers::className(), ['id' => 'Answer_set2']);
    }
     public function getReviewanswers3()
    {
        return $this->hasOne(ReviewAnswers::className(), ['id' => 'Answer_set3']);
    }
     public function getReviewanswers4()
    {
        return $this->hasOne(ReviewAnswers::className(), ['id' => 'Answer_set4']);
    }
     public function getReviewanswers5()
    {
        return $this->hasOne(ReviewAnswers::className(), ['id' => 'Answer_set5']);
    }


     public function getReviewresult()
    {
        return $this->hasMany(Reviews::className(), ['QuestionsID' => 'ID']);
    }
}