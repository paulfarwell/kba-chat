<?php
namespace backend\models;
 
use Yii;
use common\models\Chat;
 
class ReviewAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions_answers';
    }
     
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return [
    //        [['Chat','AnswersID','Question_1', 'Question_2'], 'required'],
    //         [['Question_1', 'Question_2','Question_3','Question_4','Question_5'], 'string', 'max' => 255]
            
          
    //     ];
    // }

     public function getReviewQ()
    {
        return $this->hasMany(ReviewQuestions::className(), ['Answer_set1' => 'id']);
    }
 
}