<?php

namespace backend\controllers;

use Yii;
use common\models\Industry;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class IndustriesController extends Controller
{

    public function actionIndex()
    {

        $industries = Industry::find()->all();

        return $this->render('index', ['model' => $industries]);
    }

  public function actionCreate()
    {
        $model = new Industry();
        // new record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('create', ['model' => $model]);
    }

     /**
     * Edit
     * @param integer $id
     */
    public function actionEdit($id)
    {
        $model = Industry::find()->where(['id' => $id])->one();

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // update record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('update', ['model' => $model]);
    }


    /**
     * Delete
     * @param integer $id
     */
     public function actionDelete($id)
     {
         $model = Industry::findOne($id);

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // delete record
        $model->delete();

        return $this->redirect(['index']);
     }





}
