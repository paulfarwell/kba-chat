<?php

namespace backend\controllers;

use Yii;
use common\models\Profession;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class ProfessionsController extends Controller
{

    public function actionIndex()
    {

        $professions = Profession::find()->all();

        return $this->render('index', ['model' => $professions]);
    }

  public function actionCreate()
    {
        $model = new Profession();
        // new record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('create', ['model' => $model]);
    }

     /**
     * Edit
     * @param integer $id
     */
    public function actionEdit($id)
    {
        $model = Profession::find()->where(['id' => $id])->one();

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // update record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('update', ['model' => $model]);
    }


    /**
     * Delete
     * @param integer $id
     */
     public function actionDelete($id)
     {
         $model = Profession::findOne($id);

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // delete record
        $model->delete();

        return $this->redirect(['index']);
     }





}
