<?php

namespace backend\controllers;

use Yii;
use backend\models\Slider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Reviews;
use yii\data\ActiveDataProvider;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class SliderController extends Controller
{

    public function actionIndex()
    {

        $slider = Slider::find()->all();

        return $this->render('index', ['model' => $slider]);
    }

  public function actionCreate()
    {
        $model = new Slider();
        $uploadPath = \Yii::getAlias('@frontend/web/sliders');
        // new record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          $slider1 = UploadedFile::getInstance($model, 'slider_1');
          $slider2 = UploadedFile::getInstance($model, 'slider_2');
          $slider3 = UploadedFile::getInstance($model, 'slider_3');
          $slider4 = UploadedFile::getInstance($model, 'slider_4');
          $slider5 = UploadedFile::getInstance($model, 'slider_5');
          $slider6 = UploadedFile::getInstance($model, 'slider_6');
          if (!empty($slider1)) {
              $slider_1 =  uniqid() . preg_replace('/\s+/', '_', $slider1->name);
              if ($slider1->saveAs($uploadPath . '/' . $slider1->name)) {
                  $model->slider_1= $slider1->name;
              }
          }
          if (!empty($slider2)) {
              if ($slider2->saveAs($uploadPath . '/' . $slider2->name)) {
                  $model->slider_2= $slider2->name;
              }
          }
          if (!empty($slider3)) {
              $slider_3 =  uniqid() . preg_replace('/\s+/', '_', $slider3->name);
              if ($slider3->saveAs($uploadPath . '/' . $slider3->name)) {
                  $model->slider_3= $slider3->name;
              }
          }
          if (!empty($slider4)) {
              $slider_4 =  uniqid() . preg_replace('/\s+/', '_', $slider4->name);
              if ($slider4->saveAs($uploadPath . '/' . $slider4->name)) {
                  $model->slider_4= $slider4->name;
              }
          }
          if (!empty($slider5)) {
              $slider_5 =  uniqid() . preg_replace('/\s+/', '_', $slider5->name);
              if ($slider5->saveAs($uploadPath . '/' . $slider5->name)) {
                  $model->slider_5= $slider5->name;
              }
          }
          if (!empty($slider6)) {
              $slider_6 =  uniqid() . preg_replace('/\s+/', '_', $slider6->name);
              if ($slider6->saveAs($uploadPath . '/' . $slider6->name)) {
                  $model->slider_6= $slider6->name;
              }
          }
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('create', ['model' => $model]);
    }

     /**
     * Edit
     * @param integer $id
     */
    public function actionEdit($id)
    {
        $model = Slider::find()->where(['id' => $id])->one();
        $uploadPath = \Yii::getAlias('@frontend/web/sliders');

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // update record
        if($model->load(Yii::$app->request->post()) && $model->validate()){
          $slider1 = UploadedFile::getInstance($model, 'slider_1');
          $slider2 = UploadedFile::getInstance($model, 'slider_2');
          $slider3 = UploadedFile::getInstance($model, 'slider_3');
          $slider4 = UploadedFile::getInstance($model, 'slider_4');
          $slider5 = UploadedFile::getInstance($model, 'slider_5');
          $slider6 = UploadedFile::getInstance($model, 'slider_6');
          if (!empty($slider1)) {
              $slider_1 =  uniqid() . preg_replace('/\s+/', '_', $slider1->name);
              if ($slider1->saveAs($uploadPath . '/' . $slider1->name)) {
                  $model->slider_1= $slider1->name;
              }
          }
          if (!empty($slider2)) {
              if ($slider2->saveAs($uploadPath . '/' . $slider2->name)) {
                  $model->slider_2= $slider2->name;
              }
          }
          if (!empty($slider3)) {
              $slider_3 =  uniqid() . preg_replace('/\s+/', '_', $slider3->name);
              if ($slider3->saveAs($uploadPath . '/' . $slider3->name)) {
                  $model->slider_3= $slider3->name;
              }
          }
          if (!empty($slider4)) {
              $slider_4 =  uniqid() . preg_replace('/\s+/', '_', $slider4->name);
              if ($slider4->saveAs($uploadPath . '/' . $slider4->name)) {
                  $model->slider_4= $slider4->name;
              }
          }
          if (!empty($slider5)) {
              $slider_5 =  uniqid() . preg_replace('/\s+/', '_', $slider5->name);
              if ($slider5->saveAs($uploadPath . '/' . $slider5->name)) {
                  $model->slider_5= $slider5->name;
              }
          }
          if (!empty($slider6)) {
              $slider_6 =  uniqid() . preg_replace('/\s+/', '_', $slider6->name);
              if ($slider6->saveAs($uploadPath . '/' . $slider6->name)) {
                  $model->slider_6= $slider6->name;
              }
          }
          if ($model->save()) {
              return $this->redirect(['index']);
          }
        }

        return $this->render('update', ['model' => $model]);
    }


    /**
     * Delete
     * @param integer $id
     */
     public function actionDelete($id)
     {
         $model = Slider::findOne($id);

        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // delete record
        $model->delete();

        return $this->redirect(['index']);
     }


     // public function actionShow($id){
     //
     //
     //    $model =  Slider::find()->where(['QuestionsID' => $id])->one();
     //    $dataProvider = new ActiveDataProvider([
     //        'query' => $query,
     //        'totalCount'=> Reviews::find()->count()
     //    ]);
     //
     //    return $this->render('show',['dataProvider'=>$dataProvider, 'model'=>$model]);
     // }


}
