<?php
/**
 * @link http://phe.me
 * @copyright Copyright (c) 2014 Pheme
 * @license MIT http://opensource.org/licenses/MIT
 */


use yii\widgets\ActiveForm;


/**
 * @var yii\web\View $this
 * @var pheme\settings\models\Setting $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="setting-form">
    <?php $form = ActiveForm::begin(['id' => 'theme-settings-form']); ?>

		<?= $form->field($model, 'logo') ?>
		<?= $form->field($model, 'logodescription') ?>
	<div class="form-group">
		<?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'value'=>'Create', 'name'=>'submit']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
