<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EditionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Editions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edition-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Edition', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'EditionName',
            'Theme',
            'Year',
            [                      // the owner name of the model
                'attribute'=>'Quarter',
                'label' => 'Quarter',
                'filter'=>Edition::quarterAlias(),
                'content' => function($data){
                    return $data->quarterAlias($data->Quarter);
                }
                
            ],
            [                      // the owner name of the model
                'attribute'=>'Current',
                'label' => 'Current',
                'filter'=>Edition::currentAlias(),
                'content' => function($data){
                    return $data->currentAlias($data->Current);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
