<?php
use yii\helpers\Html;
 ?>

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
        	<?= Html::img('@web/images/avatar5.png', ['class'=>"img-circle"]) ?>
        </div>
        <div class="pull-left info">
          <p><?php echo Yii::$app->user->identity->username?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>


      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu</li>
        <!-- Optionally, you can add icons to the links -->
        <li><?= Html::a( 'Users', ['/user/admin']) ?></li>
        <li><?= Html::a( 'Edition', ['/edition']) ?></li>
        <li><?= Html::a( 'Chat', ['/chat']) ?></li>
        <li><?= Html::a( 'Message', ['/message']) ?></li>
        <li><?= Html::a(  'Event', ['/event']) ?></li>
        <li><?= Html::a( 'Attachment', ['/attachment']) ?></li>
        <li><?= Html::a( 'Review Questions', ['/review']) ?></li>
        <li><?= Html::a( 'Industries', ['/industries']) ?></li>
        <li><?= Html::a( 'Professions', ['/professions']) ?></li>
        <li><?= Html::a( 'Countries',['/countries']) ?></li>
        <li><?= Html::a('Counties', ['/counties']) ?></li>
        <li><?= Html::a('Top Sliders',['/slider']) ?></li>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
