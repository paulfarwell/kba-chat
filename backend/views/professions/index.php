<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Professions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Profession', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

      <table border="1" class="table table-striped table-bordered">
    <tr>
        <th>#</th>
        <th>County Name</th>
        <th>Actions</th>
    </tr>
    <?php foreach($model as $field){ ?>
    <tr>
        <td><?= $field->id; ?></td>
        <td><?= $field->name; ?></td>
        <td><?= Html::a("Edit", ['professions/edit', 'id' => $field->id]); ?> | <?= Html::a("Delete", ['professions/delete', 'id' => $field->id]); ?></td>
    </tr>
    <?php } ?>
</table>
</div>
