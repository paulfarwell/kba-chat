<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Chat') ?>

    <?= $form->field($model, 'User') ?>

    <?= $form->field($model, 'UserName') ?>

    <?= $form->field($model, 'ChatTime') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'MessageType') ?>

    <?php // echo $form->field($model, 'MessageText') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
