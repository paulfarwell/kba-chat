<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Chat */

$this->title = 'Upload Sliders';
$this->params['breadcrumbs'][] = ['label' => 'Slider', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
