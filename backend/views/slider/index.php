<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Top Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Top Sliders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

      <table border="1" class="table table-striped table-bordered">
    <tr>
        <th>ID</th>
        <th>Slider 1</th>
        <th>Slider 2</th>
        <th>Slider 3</th>
        <th>Slider 4</th>
        <th>Slider 5</th>
        <th>Slider 6</th>
        <th>Actions</th>
    </tr>
    <?php foreach($model as $field){ ?>
    <tr>
        <td><?= $field->id; ?></td>
        <td><?= $field->slider_1?></td>
        <td><?= Html::img('@frontend/web/sliders/'. $field->slider_2) ?></td>
        <td><?= Html::img('@frontend/web/sliders/'. $field->slider_3) ?></td>
        <td><?= Html::img('@frontend/web/sliders/'. $field->slider_4) ?></td>
        <td><?= Html::img('@frontend/web/sliders/'. $field->slider_5) ?></td>
        <td><?= Html::img('@frontend/web/sliders/'. $field->slider_6) ?></td>
        <td><?= Html::a("Edit", ['slider/edit', 'id' => $field->id]); ?> | <?= Html::a("Delete", ['slider/delete', 'id' => $field->id]); ?></td>
    </tr>
    <?php } ?>
</table>
</div>
