<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Edition') ?>

    <?= $form->field($model, 'ChatDate') ?>

    <?= $form->field($model, 'StartTime') ?>

    <?= $form->field($model, 'EndTime') ?>

    <?php // echo $form->field($model, 'Topic') ?>

    <?php // echo $form->field($model, 'Host') ?>

    <?php // echo $form->field($model, 'Description') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'Log') ?>

    <?php // echo $form->field($model, 'Podcast') ?>

    <?php // echo $form->field($model, 'ParticipantsLimt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
