<?php

use yii\widgets\ListView;
use backend\models\ReviewQuestions;
use backend\models\ReviewAnswers;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\Html;


 ?>

 <div class="col-lg-12">
<?php
$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    
     [
        'attribute' => $model->questions->Question_1,
        'value' => function($model) {
                    return $model->Answer_1;
         }
    ],
    [
        'attribute' => $model->questions->Question_2,
        'value' => function($model) {
                    return $model->Answer_2;
         }
    ],
     [
        'attribute' => $model->questions->Question_3,
        'value' => function($model) {
                    if($model->Answer_3 == NULL){
                     return '';
                 }else{
                    return $model->Answer_3;
                }
         }
    ],
    
    [
        'attribute' => $model->questions->Question_4,
        'value' => function($model) {
                     if($model->Answer_4 == NULL){
                     return '';
                 }else{
                    return $model->Answer_4;
                }
         }
    ],
    [
        'attribute' => $model->questions->Question_5,
        'value' => function($model) {
                 if($model->Answer_5 == NULL){
                     return '';
                 }else{
                    return $model->Answer_5;
                }
         }
    ],
    [
        'attribute' => 'Comment',
        'value' => function($model) {
                 if($model->comment == NULL){
                     return '';
                 }else{
                    return $model->comment;
                }
         }
    ],
];
// foreach ($dataProvider->getModels() as $key => $value) {
//     var_dump($value);die;
// }

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'beforeHeader'=>[
        [
            // 'columns'=>[
            //     ['content'=>'Header Before 1', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 2', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
            //     ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
            // ],
            'options'=>['class'=>'skip-export'] // remove this row from export
        ]
    ],
    'toolbar' =>  [
        // ['content'=>
        //     // Html::button('&lt;i class="glyphicon glyphicon-plus">&lt;/i>', ['type'=>'button', 'title'=>Yii::t('', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
        //     // Html::a('&lt;i class="glyphicon glyphicon-repeat">&lt;/i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
        // ],
        '{export}',
        '{toggleData}'
    ],
    // 'pjax' => true,
    // 'bordered' => true,
    // 'striped' => false,
    
    // 'responsive' => true,
    // 'hover' => true,
     //'floatHeader' => true,
    // // 'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
     'showPageSummary' => true,
     'panel' => [
         'type' => GridView::TYPE_PRIMARY
     ],
]);
?>
</div>