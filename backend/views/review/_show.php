<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">
    <h2><?= Html::encode($model->Question_1) ?></h2>

    <?= HtmlPurifier::process($model->Question_2) ?>    
</div>