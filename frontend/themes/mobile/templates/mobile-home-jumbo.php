<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Chat;

use Zelenin\yii\SemanticUI\modules\Modal;
$settings = Yii::$app->settings;
$settings->clearCache();
$value = $settings->get('Theme.homePageDescription');
$description = $settings->get('Theme.homePa');
// Automatically called on set();
?>

<!-- home-jumbo -->
<div class="background-over">
<header >
  <div class="container">
    <div class="row">


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
      <div class="top_menu">


    <div class="col-xs-4">
      <?php if (Yii::$app->user->isGuest):?>

  <?= Html::img('@web/images/icons/pen.png',['class' => 'img-responsive','style'=>' height:20px; display:inline;'])?>&nbsp; <a href="<?= Url::toRoute(['site/register#register',])?>" >REGISTER </a>

   <?php else:?>

       <?= Html::img('@web/images/icons/chat.png',['class' => 'img-responsive','style'=>' height:20px; display:inline'])?>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a>
   <?php endif;?>
    </div>
    <div class="col-xs-4">
      <?php if (Yii::$app->user->isGuest):?>

        <?= Html::img('@web/images/icons/lock.png',['class' => 'img-responsive','style'=>' height:20px; display:inline;'])?>&nbsp;<a href="<?= Url::toRoute(['site/register#login'])?>" >LOGIN</a>

     <?php else:?>
    
        <?= Html::img('@web/images/icons/profile-white.png',['class' => 'img-responsive','style'=>' height:20px; display:inline;'])?>&nbsp;<a href="<?= Url::to(['/user/settings/myprofile'])?>" data-method="post" >MY PROFILE</a>


     <?php endif;?>
    </div>
    <div class="col-xs-4">
      <?php if (Yii::$app->user->isGuest):?>

          <?= Html::img('@web/images/icons/chat.png',['class' => 'img-responsive','style'=> 'height:20px; display:inline;'])?>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a>
        <?php else:?>
               <?= Html::img('@web/images/icons/power-white.png',['class' => 'img-responsive','style'=>' height:20px; display:inline;'])?>&nbsp;<a href="<?= Url::to(['/user/security/logout'])?>" data-method="post" >LOGOUT</a>

     <?php endif;?>
    </div>
      </div>

    <!-- <ul class="top_menu">
       <?php if (Yii::$app->user->isGuest):?>

    <li><?= Html::img('@web/images/icons/pen.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?></li><li >&nbsp; <a href="<?= Url::toRoute(['site/register#register',])?>" >REGISTER</a><p>&nbsp;and create an account</p></li>

    <?php else:?>

        <li><?= Html::img('@web/images/icons/chat.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?></li><li>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>&nbsp;and share your insights with the team</p> </li>

    <?php endif;?>
     <?php if (Yii::$app->user->isGuest):?>

        <li><?= Html::img('@web/images/icons/lock.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?></li><li>&nbsp;<a href="<?= Url::toRoute(['site/register#login'])?>" >LOGIN</a><p>&nbsp; and join the conversation</p></li>

    <?php else:?>

          <li><?= Html::img('@web/images/icons/lock.png',['class' => 'img-responsive','style'=>' height:40px; display:inline-flex;margin-top:-10px'])?></li><li>&nbsp;<a href="<?= Url::to(['/user/security/logout'])?>" data-method="post" >LOGOUT</a></li>


           <i class="fa fa-user "></i><li ><a href="<?= Url::to(['/user/settings/myprofile'])?>" data-method="post" >MY PROFILE</a></li>

    <?php endif;?>
     <?php if (Yii::$app->user->isGuest):?>

         <li><?= Html::img('@web/images/icons/chat.png',['class' => 'img-responsive','style'=> 'height:30px; display:inline-flex;margin-top:-40px'])?></li><li>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>&nbsp;and share your insights with the team</p> </li>

    <?php endif;?>
    </ul> -->
    </div>
    </div>
    <div class="row">


      <div class="intro-text">
        <?php
            $words = explode(' ',$description);
            ?>
           <div class="intro-heading"><?= implode(' ',array_slice($words,0,6))?></div>
          <h1 class="intro-lead-in"><?= implode(' ',array_slice($words,7))?></h1>
              <div class="bounce-holder">
                  <span class="icon-down"></span>
              </div>
          </div>
          <!-- <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a> -->
  </div>
</div>

</header>

</div>
<?= $this->render('@frontend/views/templates/metro') ?>



<!--  -->
<!-- ends home-jumbo -->
