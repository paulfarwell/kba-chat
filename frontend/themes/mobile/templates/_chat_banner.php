<?php
use yii\helpers\Html;
use backend\models\Slider;
$settings = Yii::$app->settings;
$settings->clearCache();
$logo = $settings->get('Theme.logo');
$banner = $settings->get('Theme.banner');
$sliders = Slider::find()->all();
?>
<nav class="navbar navbar-inverse ">
  <div class="container">


    <div class="navbar-header">
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="navbar-brand"><?php echo Html::img('@web/images/site/'. $logo, ['class' => ' img-responsive','style'=>'padding-left:15px']); ?></div>
    </div>
     <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6  ">

<div id="slider">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!-- <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
  </ol> -->


  <?php foreach($sliders as $slider):?>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <?php echo Html::img('@web/images/site/top.png', ['class' => 'slider-image pull-right']); ?>
      </div>
      <!-- <?php if(!empty($slider->slider_2)):?>
      <div class="item">
      <?php echo Html::a(Html::img('@web/banner/'.$slider->slider_2, ['class' => 'slider-image pull-right']), ['/chat']); ?>
      </div>
    <?php elseif(!empty($slider->slider_3)):?>
       <div class="item">
      <?php echo Html::a(Html::img('@web/banner/'.$slider->slider_3, ['class' => ' slider-image pull-right']), ['/chat']); ?>
      </div>
    <?php elseif(!empty($slider->slider_4)):?>
       <div class="item">
      <?php echo Html::a(Html::img('@web/banner/'.$slider->slider_4, ['class' => '  slider-image pull-right']), ['/chat']); ?>
      </div>
    <?php elseif(!empty($slider->slider_5)):?>
       <div class="item">
      <?php echo Html::a(Html::img('@web/banner/'.$slider->slider_5, ['class' => '  slider-image pull-right']), ['/chat']); ?>
      </div>
    <?php elseif(!empty($slider->slider_6)):?>
       <div class="item">
      <?php echo Html::a(Html::img('@web/banner/'.$slider->slider_6, ['class' => '  slider-image pull-right']), ['/chat']); ?>
      </div>
    <?php endif;?> -->
    </div>

  <?php endforeach;?>
  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
          </div>
          </div>
        </div>
<div class="col-xs-3 col-md-3 col-sm-3 col-lg-3">
           <!-- <p class="chatbanner_follow">Follow us on <br>social media</p> -->
           <div id="social">
             <ul>
               <li class="fb"><a href="https://www.facebook.com/KenyaBankersAssociation" target="_blank" class="j-media"></a></li>
               <li class="ln"><a href="http://www.linkedin.com/groups/Kenya-Bankers-Association-4483366" target="_blank" class="j-media"></a></li>
               <li class="tw"><a href="https://twitter.com/KenyaBankers" target="_blank" class="j-media"></a></li>
               <li class="yt"><a href="https://www.youtube.com/channel/UC7iBaNT752oNRQohVBiQVRQ" target="_blank" class="j-media"></a></li>
             </ul>

           </div>
        </div>

</nav>
  </div>
<div class="clearfix"></div>
