$(function(){
    $('#mention-notificationsBody').slimScroll({
        height: '250px'
    });
});
$("#mention-notificationLink").click(function()
{
	$("#mention-notificationContainer").fadeToggle(300);
	$("#mention-notificationsBody" ).last().addClass( "mention-notification_seen" );
	$("#mention-notification_count").fadeOut("slow");
	return false;
});
//Document Click hiding the popup 
$(document).click(function()
{
$("#mention-notificationContainer").hide();
$("div.mention-notification_seen > div.mention-notification_single").remove();
});

//Popup on click
$("#mention-notificationContainer").click(function()
{
return false;
});

