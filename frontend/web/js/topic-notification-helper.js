$(function(){
    $('#topic-notificationsBody').slimScroll({
        height: '150px'
    });
});
$("#topic-notificationLink").click(function()
{
	$("#topic-notificationContainer").fadeToggle(300);
	$("#topic-notificationsBody" ).last().addClass( "topic-notification_seen" );
	$("#topic-notification_count").fadeOut("slow");
	return false;
});
//Document Click hiding the popup 
$(document).click(function()
{
$("#topic-notificationContainer").hide();
$("div.topic-notification_seen > div.topic-notification_single").remove();
});

//Popup on click
$("#topic-notificationContainer").click(function()
{
return false;
});

$('#my_host_close').click(function() {
        $("#host-notificationContainer").hide();
$("div.host-notification_seen > div.host-notification_single").remove();
    });

$('#my_topic_close').click(function() {
        $("#topic-notificationContainer").hide();
$("div.topic-notification_seen > div.topic-notification_single").remove();
    });