$(document).ready(function(){
	// smooth scroll
	$('a[href*="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});


	// toggle active and online users on chat 	
	$('#toggle_userslist').click(function() {
	    $('#users_active, #users_online').toggle();
	    $(this).text(function(i,txt) {
	        return txt === "Active users" ? "Online Users" : "Active users";
	    });
	});


	  $('#chat-body, #area_filtered_chat').perfectScrollbar();


	   $(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });

});
