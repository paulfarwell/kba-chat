<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace frontend\controllers\user;

use dektrium\user\Finder;
use dektrium\user\models\SettingsForm;
use dektrium\user\Module;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

use dektrium\user\controllers\SettingsController as BaseSettingsController;

class SettingsController extends BaseSettingsController
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['profile', 'account', 'confirm', 'networks', 'disconnect','myprofile'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
          
    }

    public function actionMyprofile()
    {
        $model = $this->finder->findProfileById(Yii::$app->user->identity->getId());
        return $this->render('myprofile', [
            'model' => $model,
        ]);
    }
}