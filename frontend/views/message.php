<?php
use yii\helpers\Html;
use yii\helpers\Url;


/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module

 */


$modalJs = <<< 'JS'
		$('#modalSuccess').modal('toggle');
JS;
$this->registerJs($modalJs, \yii\web\View::POS_READY);
?>
<?php if( isset($redirect)):?>

<?php

$redirectUrl = 'var redirectUrl = "' . $redirect . '";';
$this->registerJs($redirectUrl, \yii\web\View::POS_HEAD, 'my-inline-js');
$modalredirect = <<< 'JS'
	$('#modalSuccess').on('hidden.bs.modal', function () {
	    window.location = redirectUrl;
	})
JS;
$this->registerJs($modalredirect, \yii\web\View::POS_READY);
?>
<?php endif;?>
<?php if(Yii::$app->session->getAllFlashes()):?>
<div class="modal remote fade" id="modalSuccess">
        <div class="modal-dialog modal-lg">
            <div class="modal-content loader-lg">
            	<div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal">&times;</button>
				 </div>
				 <div class="modal-body">
				 		<div class="text-center">
				     		<?= $this->render('/_confirmalert'); ?>
				     	</div>
				 </div>
            </div>
        </div>
</div>
<?php endif;?>
