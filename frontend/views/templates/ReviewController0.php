<?php

namespace backend\controllers;

use Yii;
use backend\models\ReviewQuestions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Reviews;
use yii\data\ActiveDataProvider;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class ReviewController extends Controller
{

    public function actionIndex()
    {
      

        $review = ReviewQuestions::find()->all();
         
        return $this->render('index', ['model' => $review]);
    }

  public function actionCreate()
    {
        $model = new ReviewQuestions();
 
        // new record
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
                 
        return $this->render('create', ['model' => $model]);
    }

     /**
     * Edit
     * @param integer $id
     */
    public function actionEdit($id)
    {
        $model = ReviewQuestions::find()->where(['id' => $id])->one();
 
        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');
         
        // update record
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
         
        return $this->render('update', ['model' => $model]);
    }  


    /**
     * Delete
     * @param integer $id
     */
     public function actionDelete($id)
     {
         $model = ReviewQuestions::findOne($id);
         
        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');
             
        // delete record
        $model->delete();
         
        return $this->redirect(['index']);
     } 


     public function actionShow($id){

        $query =  Reviews::find()->where(['QuestionsID' => $id]);
        $model =  Reviews::find()->where(['QuestionsID' => $id])->one();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount'=> Reviews::find()->count()
        ]);
        
        return $this->render('show',['dataProvider'=>$dataProvider, 'model'=>$model]);
     }


}
