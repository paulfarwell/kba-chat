<!-- Chat details
================================================== -->

<section class="chat-details-container container-orange">
	<div class="container"> 
	    <div class="row">
	        
	        <div class="col-md-6">

	            <?php include 'login-form.php';?>
	        </div><!-- col-md-6 -->


	        <div class="col-md-6">

	            <?php include 'ceo-chat.php';?>
	        </div><!-- col-md-6 -->

	        <!-- <div class="clearfix"></div> -->

	    </div><!-- main row -->

	</div> <!-- main container -->    
</section> <!-- container-section -->