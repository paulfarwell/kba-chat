<?php
use yii\helpers\Html;
use backend\models\Slider;
use yii\helpers\Url;
use common\models\Chat;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\Elements;
?>\
<div class="col-xs-12 col-md-12 ">
<div id="event_topics_slider" class="chat_summary_slider">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="9000">

 <!-- Wrapper for slides -->
 <div class="carousel-inner" role="listbox" style="margin:auto; width:85%">
   <div class="item active">
     <div class="col-md-4 col-xs-12">
       <div class="event_holder">
         <?php echo Html::img('@web/images/icons/interest-rate.png', ['class' => 'slider-image ']); ?>
         <div class="caption">
           <h1>INTEREST RATES</h1>
           <p>Why banks charge the way they do</p>

         </div>
       </div>

     </div>
     <div class="col-md-4 col-xs-12">
       <div class="event_holder">
         <?php echo Html::img('@web/images/icons/debt.png', ['class' => 'slider-image ']); ?>
         <div class="caption">
           <h1>GOOD DEBT VS BAD DEBT</h1>
           <p>When should you apply for a loan</p>

         </div>
       </div>
     </div>
     <div class="col-md-4 col-xs-12">
       <div class="event_holder">
         <?php echo Html::img('@web/images/icons/finance-home.png', ['class' => 'slider-image ']); ?>
         <div class="caption">
           <h1>HOW TO FINANCE YOUR FIRST HOME</h1>
           <p>Points to dicuss with your bank</p>

         </div>
       </div>
     </div>
   </div>
   
 </div>
 <!-- Controls -->
 <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
   <span class="sr-only">Previous</span>
 </a>
 <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
   <span class="sr-only">Next</span>
 </a>
</div>
</div>
</div>
