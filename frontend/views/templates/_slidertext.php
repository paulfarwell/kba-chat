<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Chat;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\Elements;
$settings = Yii::$app->settings;
$settings->clearCache();
$value = $settings->get('Theme.logo');
$ban = [];
?>

<div class="intro-text">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <!-- slider-chat-events -->
<div class=" carousel-body events-slider carousel-section container-fluid" style="margin-top:-30px;">
<h3 class="carousel-section-head"><b>UPCOMING CHAT DETAILS:</b></h3>
<div class="event-slider-content" id="events_section">
<?php foreach ($chats as $chat): ?>

   <table class="table-slide">

        <tr> <td><h3 style="text-transform:initial; "><b>Topic:</b></h3></td> <td><h3 style="text-transform:initial; "><?php echo $chat->Topic;?></h3></td></tr>
        <tr> <td><h3 style="text-transform:initial; "><b>Date:</b></h3></td> <td><h3 style="text-transform:initial; "><?php echo $chat->ChatDate;?></h3></td></tr>
        <tr> <td><h3 style="text-transform:initial; "><b>Time:</b></h3></td> <td><h3 style="text-transform:initial; "><?= Html::encode(Yii::$app->formatter->asTime($chat->StartTime, "HH:mm a")." - ".Yii::$app->formatter->asTime($chat->EndTime, "HH:mm a")); ?></h3></td></tr>
        <tr> <td><h3 style="text-transform:initial; "><b>Host:</b></h3></td> <td><h3 style="text-transform:initial; "><?php echo $chat->ceostring;?></h2></h3></td></tr>
            <tr> <td>&nbsp;&nbsp;</td> <td>&nbsp;&nbsp;</td></tr>
        <tr><td><?= Html::a('Topic Details', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->Topic . "Details",  'class' => "btn btn-join",]) ?>  </td>
          <td><?= Html::a('About The Host', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->ceostring . "Details",  'class' => "btn btn-join", 'style'=>'margin-left:5px']) ?></td></tr>
    </table>

<?php endforeach;?>
</div>
</div>
<!-- slider-chat-events -->
        </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 ">
    <ul class="pull-right kcb-menu">
       <?php if (Yii::$app->user->isGuest):?>
    <?= Html::img('@web/images/icons/front/register.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?><li>&nbsp; <a href="<?= Url::toRoute(['site/register#register',])?>" >REGISTER</a><p>&nbsp;and create an account</p></li><hr>
    <?php else:?>
        <?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?><li>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>&nbsp;and share your insights with the team</p> </li><hr>
    <?php endif;?>
     <?php if (Yii::$app->user->isGuest):?>
        <li><?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>' height:30px; display:inline-flex;margin-top:-40px'])?><li>&nbsp;<a href="<?= Url::toRoute(['site/register#login'])?>" >LOGIN</a><p>&nbsp; and join the conversation</p></li><hr>
    <?php else:?>
          <?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>' height:40px; display:inline-flex;margin-top:-10px'])?><li>&nbsp;<a href="<?= Url::to(['/user/security/logout'])?>" data-method="post" >LOGOUT</a></li><hr>
           <i class="fa fa-user "></i><li ><a href="<?= Url::to(['/user/settings/myprofile'])?>" data-method="post" >MY PROFILE</a></li><hr>
    <?php endif;?>
     <?php if (Yii::$app->user->isGuest):?>
         <?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=> 'height:30px; display:inline-flex;margin-top:-40px'])?><li>&nbsp;<a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>&nbsp;and share your insights with the team</p> </li><hr>
      <i class="fa fa-calendar" aria-hidden="true"></i>  <li>
                 <?php $requestDemo = Modal::begin([
               'size' => Size::SMALL,
               'header' => 'Request Demo',
               'actions' => Elements::button('Close' . Elements::icon('remove'), ['class' => 'cancel right labeled icon']),

           ]); ?>
         <?= $this->render('@frontend/views/site/requestDemo')?>

         <?php $requestDemo::end(); ?>

          <?= $requestDemo->renderToggleButton('Request a Demo',['style'=>'margin-left:-30px;text-transform:uppercase']) ?>
       </li>
   <?php else:?>
      <i class="fa fa-calendar" aria-hidden="true"></i><li>
             <?php $requestDemo = Modal::begin([
           'size' => Size::SMALL,
           'header' => 'Request Demo',
           'actions' => Elements::button('Close' . Elements::icon('remove'), ['class' => 'cancel right labeled icon']),

       ]); ?>
     <?= $this->render('@frontend/views/site/requestDemo')?>

     <?php $requestDemo::end(); ?>

     <?= $requestDemo->renderToggleButton('Request a Demo',['style'=>'margin-left:-30px;text-transform:uppercase']) ?>
   </li>
   <?php endif;?>
    </ul>
    </div>


    <!-- <div class="intro-heading">Welcome To Our Studio!</div> -->
    </div>
