<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <div class="container">
      <div class="navbar-header">
        <h1 class="nav-brand"> <?= Html::img('@web/images/icons/profile-white.png',['class' => 'img-responsive','style'=>' height:30px; display:inline;margin-top:-5px'])?> My Profile</h1>

    </div>
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?= Url::toRoute(['/',])?>" ><span class="fa fa-home"></span>Home</a></li>
  <li><a href="<?= Url::toRoute(['/chat/enter'])?>"><span class="fa fa-comment-o"></span>Enter Chat Room</a> </li>
    <?php if (Yii::$app->user->isGuest):?>
    <li ><a href="<?= Url::toRoute(['site/register#login'])?>" ><span class="fa fa-lock "></span>Login</a></li>
     <li ><a href="<?= Url::toRoute(['site/register#register'])?>" ><span class="fa fa-pencil "></span>Register</a></li>
  <?php else:?>
    <li ><a href="<?= Url::to(['/chat/archives'])?>" ><span class="fa fa-archive " data-method="post"></span>Chat Archive</a></li>
    <li ><a href="<?= Url::to(['/user/security/logout'])?>" data-method="post" ><span class="fa fa-power-off " ></span>Logout</a></li>

  <?php endif;?>
   <li><a href="<?= Url::to(['/site/help'])?>"><span class="fa fa-question"></span>Help</a></li>

      </ul>
    </div>
    </div>
  </div>
</nav>
