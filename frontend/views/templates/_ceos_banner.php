<?php
use yii\helpers\Html;
$this->registerCssFile('/css/slick.css');
$this->registerCssFile('/css/slick-theme.css');
$this->registerJsFile('/js/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS
 $('#ceos_section').slick({
  infinite: true,
  slidesToShow: 7,
  slidesToScroll: 7,
  autoplay: true,
  autoplaySpeed: 6000,
  dots: false,
  arrows: false,
  centerPadding: '0px',
  responsive: [
        {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
    ]
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

<div class="ceo-banners carousel-section row ">
    <div class="col-sm-2 col-xs-3">
      <div class=" previous-chats"> 
      <div class="dummy"></div>

          <div class="img-container">
              <div class="centerer"></div>
              <?php echo Html::a(Html::img('@web/images/prev-chats.png', ['class' => 'img-responsive']), ['/chat/archives']); ?>
          </div>
      </div>
    </div>

    <div class="ceos_section col-sm-10 col-xs-9" id="ceos_section">
        
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/1.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/2.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/3.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/7.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/10.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/5.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/12.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/8.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/4.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>
        <span class="ceo_thumbnail">
            <?=  Html::img('@web/images/6.jpg', ['class' => 'img-responsive']) ?>
            <div class="ceo_info"> 
                <h2 class="carousel-section-head">LOREM Ipsum</h2><p class="company">Housing Finance</p>
            </div>
        </span>


    </div> <!-- ceos_section -->
</div>  

