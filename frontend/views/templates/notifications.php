<?php
$this->registerJsFile('/js/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/notification-helper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
use yii\helpers\Html;
?>
<div class="notifications-wrapper">
	
	<ul id="nav" class="notifications_ul">
		<li id="notification_li" class="pull-right">
		<button class="btn btn-default btn-lg btn-link pull-right" style="font-size:24px;" id="notificationLink">
		    <?php echo Html::a(Html::img('@web/images/icons/notification-icon.png', ['class' => ' img-responsive', 'style'=>'width:40px;height:40px'])); ?>
		    <span class="badge badge-notify" id="notification_count"></span>
		</button>
		
	<!--
		<a href="#" id="notificationLink"><span id="notification_count">3</span><i class="nav-icon fa fa-bullhorn"></i> 
		<span class="small-screen-text">Notifications</span></a>-->

		<div id="notificationContainer">
			
		<!--<div id="notificationTitle">New Messages</div>-->
		<div id="notificationsBody" class="notifications"></div>
		<!--<div id="notificationFooter"><a href="#">See All</a></div>-->
		</div>

		</li>
	</ul>
</div>