<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;


/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module

 */


$modalJs = <<< 'JS'
		$('#modalSuccess').modal('toggle');
JS;
$this->registerJs($modalJs, \yii\web\View::POS_READY);
?>
<?php if( isset($redirect)):?>

<?php

$redirectUrl = 'var redirectUrl = "' . $redirect . '";';
$this->registerJs($redirectUrl, \yii\web\View::POS_HEAD, 'my-inline-js');
$modalredirect = <<< 'JS'
	$('#modalSuccess').on('hidden.bs.modal', function () {
	    window.location = redirectUrl;
	})
JS;
$this->registerJs($modalredirect, \yii\web\View::POS_READY);
?>
<?php endif;?>
<?php if (Yii::$app->session->hasFlash('success') || Yii::$app->session->hasFlash('danger') || Yii::$app->session->hasFlash('info')):?>
				 		<div class="text-center">
							    <div class="row flashes">
							        <div class="col-xs-12">
							            <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
							                <?php if (in_array($type, ['success', 'danger', 'info'])): ?>
							                    <div class="alert alert-<?= $type ?>">
							                        <?= $message ?>
							                    </div>
							                <?php endif ?>
							            <?php endforeach ?>
							        </div>
							    </div>
</div>

<?php endif;?>
