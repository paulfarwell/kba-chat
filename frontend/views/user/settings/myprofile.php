<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use dektrium\user\widgets\Connect;
use Zelenin\yii\SemanticUI\Elements;
/*
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */
// echo '<pre>',print_r($model),'</pre>';
// die();

$this->title = Yii::t('user', ' Profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@frontend/views/templates/profile_menu')?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="row user_profile">
<span class="page-title">
  <!-- <span class="col-xs-12 col-sm-10"><h1 class="page-header"><?= Html::encode($this->title)?></h1></span> -->

  <!-- <span class="loginout col-xs-12 col-sm-2">
    <?php if (Yii::$app->user->isGuest):?>
      <a class="btn-login" href="<?= Url::to(['site/register', '#' => 'login'])?>"> Log In </a>
      <?php else:?>
      <a class="btn-login" href="<?= Url::to(['user/security/logout'])?>"  data-method="post"> Log Out  </a>
    <?php endif; ?>

  </span> --><!--  loginout -->
</span> <!-- page-title -->

    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">

            <div class="profile_view">

              <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                <div  class="profile-view-hr">
                <span class="make-responsive">
                <?php if($model->avatar):?>
                    <div class="image_holder" style="margin-left:40%">

                    <?= Html::img('@web/images/avatar/'.$model->avatar, ['class' => 'img-responsive']); ?>
                      </div>
                <?php else:?>
                  <div class="image_holder" style="margin-left:40%">
                       <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                    </div>
                <?php endif;?>
                </span>
              </div>
              </div>

              <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">

                <ul class="profile-view-ul">
                <li>
                  <?php if ($model->name): ?>
                     <label ><?= $model->getAttributeLabel('First Name:') ?></label>
                      <span ><?= ucfirst($model->name) ?></span>
                  <?php else:?>
                      <label > <?= $model->getAttributeLabel('name:') ?></label>

                  <?php endif; ?>

                </li>
                <li>
                  <?php if ($model->lastname): ?>
                        <label ><?= $model->getAttributeLabel('Last Name:') ?> </label>
                        <span ><?= ucfirst($model->lastname) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('lastname:') ?></label>

                  <?php endif; ?>
                </li>
                <li>
                   <?php if ($model->organization): ?>
                 <label ><?= $model->getAttributeLabel('Bank:') ?> </label>
                        <span ><?= ucfirst($model->organization) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('organization:') ?></label>

                  <?php endif; ?>
                </li>
                <li>
                  <?php if ($model->designation): ?>
                  <label ><?= $model->getAttributeLabel('Branch:') ?> </label>
                        <span ><?= ucfirst($model->designation) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('designation:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if (Yii::$app->user->identity->email): ?>
                  <label ><?= $model->getAttributeLabel('Email:') ?> </label>
                        <span ><?= ucfirst(Yii::$app->user->identity->email) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('Email:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if($model->userindustry):?>
                  <label ><?= $model->getAttributeLabel('Industry:') ?> </label>
                        <span ><?= ucfirst($model->userindustry->name) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('Industry:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if($model->userprofession):?>
                  <label ><?= $model->getAttributeLabel('Profession:') ?> </label>
                        <span ><?= ucfirst($model->userprofession->name) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('profession:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if($model->usercountry):?>
                  <label ><?= $model->getAttributeLabel('Country:') ?> </label>
                        <span ><?= ucfirst($model->usercountry->CountryName) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('country:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if($model->usercounty):?>
                  <label ><?= $model->getAttributeLabel('County:') ?> </label>
                        <span ><?= ucfirst($model->usercounty->name) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('county:') ?></label>

                  <?php endif; ?>
                  </li>
                  <li>
                  <?php if ($model->city): ?>
                  <label ><?= $model->getAttributeLabel('County/Province:') ?> </label>
                        <span ><?= ucfirst($model->city) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('city:') ?></label>

                  <?php endif; ?>
                   </li>
                   <li>
                  <?php if ($model->bio): ?>
                  <label ><?= $model->getAttributeLabel('Bio:') ?> </label>
                        <span ><?= ucfirst($model->bio) ?></span>
                  <?php else:?>
                      <label ><?= $model->getAttributeLabel('bio:') ?></label>

                  <?php endif; ?>
                   </li>
                   </ul>
              </div>



                    <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-4 col-lg-offset-4">
                        <span class="make-responsive">
                        <?php if($model->avatar):?>
                            <div class="image_holder">

                            <?= Html::img('@web/images/avatar/'.$model->avatar, ['class' => 'img-responsive']); ?>
                              </div>
                        <?php else:?>
                          <div class="image_holder">
                               <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                            </div>
                        <?php endif;?>
                        </span>
                    </div>

                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-md-offset-2 col-lg-offset-2 ">
                          <hr class="profile-view-hr">
                      <ul class="profile-view-ul">
                      <li>
                        <?php if ($model->name): ?>
                           <label ><?= $model->getAttributeLabel('First Name:') ?></label>
                            <span ><?= ucfirst($model->name) ?></span>
                        <?php else:?>
                            <label > <?= $model->getAttributeLabel('name:') ?></label>

                        <?php endif; ?>

                      </li>
                      <li>
                        <?php if ($model->lastname): ?>
                              <label ><?= $model->getAttributeLabel('Last Name:') ?> </label>
                              <span ><?= ucfirst($model->lastname) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('lastname:') ?></label>

                        <?php endif; ?>
                      </li>
                      <li>
                         <?php if ($model->organization): ?>
                       <label ><?= $model->getAttributeLabel('Bank:') ?> </label>
                              <span ><?= ucfirst($model->organization) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('organization:') ?></label>

                        <?php endif; ?>
                      </li>
                      <li>
                        <?php if ($model->designation): ?>
                        <label ><?= $model->getAttributeLabel('Branch:') ?> </label>
                              <span ><?= ucfirst($model->designation) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('designation:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if (Yii::$app->user->identity->email): ?>
                        <label ><?= $model->getAttributeLabel('Email:') ?> </label>
                              <span ><?= ucfirst(Yii::$app->user->identity->email) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Email:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if($model->userindustry):?>
                        <label ><?= $model->getAttributeLabel('Industry:') ?> </label>
                              <span ><?= ucfirst($model->userindustry->name) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Industry:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if($model->userprofession):?>
                        <label ><?= $model->getAttributeLabel('Profession:') ?> </label>
                              <span ><?= ucfirst($model->userprofession->name) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('profession:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if($model->usercountry):?>
                        <label ><?= $model->getAttributeLabel('Country:') ?> </label>
                              <span ><?= ucfirst($model->usercountry->CountryName) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('country:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if($model->usercounty):?>
                        <label ><?= $model->getAttributeLabel('County:') ?> </label>
                              <span ><?= ucfirst($model->usercounty->name) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('county:') ?></label>

                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if ($model->city): ?>
                        <label ><?= $model->getAttributeLabel('County/Province:') ?> </label>
                              <span ><?= ucfirst($model->city) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('city:') ?></label>

                        <?php endif; ?>
                         </li>
                         <li>
                        <?php if ($model->bio): ?>
                        <label ><?= $model->getAttributeLabel('Bio:') ?> </label>
                              <span ><?= ucfirst($model->bio) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('bio:') ?></label>

                        <?php endif; ?>
                         </li>
                         </ul>
                     form-horizontal form-white

                </div> -->

            </div>

        </div>

   </div><!--  container -->
</div><!-- user_profile -->
