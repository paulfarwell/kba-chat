<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use sjaakp\illustrated\Uploader;
use yii\helpers\ArrayHelper;
use common\models\Countries;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use common\models\Industry;
use common\models\Profession;
use common\models\County;


/*
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */

$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@frontend/views/templates/profile_menu')?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="row user_profile">


    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>


        <div class="col-md-2">

            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9">
            <div class="profile_edit ">

                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'id' => 'profile-form',
                        'options' => ['class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-sm-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-9\">{error}\n{hint}</div>",
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        ],
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'validateOnBlur'         => false,
                    ]); ?>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-md-offset-4 col-lg-offset-4 photo_uploader">
                        <?= $form->field($model, 'avatar')->widget(Uploader::className(), [
                            'stylefileOptions' => [
                                'btnClass' => 'btn btn-upload',
                                'btnText' => '<i class="fa fa-search"></i> Browse for image'
                            ],
                            'cropperOptions' => [
                                'diagonal' => 240,
                                'margin' => 5,
                                'sliderPosition' => 'right',
                                'sliderOptions' => [
                                    'animate' => 'slow'
                                ]
                            ],
                            'deleteOptions' => [
                                'label' => '<i class="fa fa-trash"></i>', // Font Awesome icon
                                'title' => 'Delete image'
                            ],
                        ]

                    )->label(false); ?>

                    </div>

                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-md-offset-2 col-lg-offset-2 ">

                        <?= $form->field($model, 'name') ?>

                        <?= $form->field($model, 'lastname') ?>

                        <!--<?= $form->field($model, 'public_email') ?>

                        <?= $form->field($model, 'website') ?>-->
                        <?= $form->field($model, 'industry')
                                ->dropDownList(
                                    ArrayHelper::map(Industry::find()->orderBy('name')->all(), 'id', 'name'),
                                    ['prompt'=>'Select Industry']
                                );
                         ?>
                         <?= $form->field($model, 'profession')
                                 ->dropDownList(
                                     ArrayHelper::map(Profession::find()->orderBy('name')->all(), 'id', 'name'),
                                     ['prompt'=>'Select Your Profession']
                                 );
                          ?>
                        <?= $form->field($model, 'organization') ?>

                        <?= $form->field($model, 'designation') ?>

                        <!-- <?= $form->field($model, 'gravatar_email')->hint(\yii\helpers\Html::a(Yii::t('user', 'Change your avatar at Gravatar.com'), 'http://gravatar.com')) ?> -->

                        <?= $form->field($model, 'bio')->textarea() ?>

                        <?= $form->field($model, 'country')
                                                ->dropDownList(
                                                    ArrayHelper::map(Countries::find()->orderBy('CountryName')->all(), 'ID', 'CountryName'),
                                                    ['prompt'=>'Select Country']
                                                );
                                         ?>
                         <?= $form->field($model, 'county')
                                 ->dropDownList(
                                     ArrayHelper::map(County::find()->orderBy('name')->all(), 'id', 'name'),
                                     ['prompt'=>'Select County']
                                 );
                          ?>
                        <?= $form->field($model, 'city') ?>


                        <div class="form-group">
                            <div class="col-lg-2">
                                <?= \yii\helpers\Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-join']) ?><br>
                            </div>
                        </div>

                    </div>
                        <?php \yii\widgets\ActiveForm::end(); ?>

            </div>
        </div>

   </div><!--  container -->
</div><!-- user_profile -->
