<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */
$settings = Yii::$app->settings;
$logininfo = $settings->get('Theme.loginDescription');
use dektrium\user\widgets\Connect;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */


$this->title = Yii::t('user', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-login chat-details-container container-orange"  id="user_login">

    <div class="container section-login spacer-sm"> 

        <div class="row">

            <div class="col-md-6" id="login">

                <div class="login-form form-white" id="login_section">
                    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                                
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php $form = ActiveForm::begin([
                        'id'                     => 'login-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'validateOnBlur'         => false,
                        'validateOnType'         => false,
                        'validateOnChange'       => false,
                        'class' => 'form-horizontal',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'label' => 'col-xs-12 col-sm-3',
                                'offset' => 'col-xs-offset-4',
                                'wrapper' => 'col-xs-12 col-sm-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
                    ]) ?>

                    <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']])->label('Email Address') ?>

                    <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') ) ?>

                     <div class="clearfix"> </div>
                    
                    <div class="col-tight-left col-sm-6">
                        <div class="pull-left">
                     <?=  ($module->enablePasswordRecovery ? Html::submitButton( Html::a(Yii::t('user', 'Reset password'), ['/user/recovery/request']), ['class' => 'btn btn-default', 'tabindex' => '3']) : '')?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="pull-right">
                    <?= Html::submitButton(Yii::t('user', 'join Chat'), ['class' => 'btn btn-default join-chat', 'tabindex' => '3']) ?>
                        </div>
                    </div>
                    <div class="clearfix"> </div>

                    <?php ActiveForm::end(); ?>
                
                    <?php if ($module->enableConfirmation): ?>
                        <p>
                            <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend'], ['class'=> 'make-block']) ?>
                        </p>
                    <?php endif ?>
                    <?php if ($module->enableRegistration): ?>
                        <p>
                            <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
                        </p>
                    <?php endif ?>
                        
                </div> <!-- login_section -->


             </div><!-- col-md-6 -->

                
             <div class="col-md-6">
                
                <div class="about-chat">
                        <h3 class="bordered-title">CEO CHAT </h3>

                    <div class="chat-lead">
                        <?= $logininfo ?> 
                    </div>
                </div>

             </div><!-- col-md-6 -->


        </div><!-- main row -->

    </div> <!-- main container -->    
</section> <!-- container-section -->
