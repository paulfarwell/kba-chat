<?php
/**
 * var $model  user model
 * var $profile the user profile model
 */
 //print_r($model);
use Zelenin\yii\SemanticUI\Elements;
use yii\helpers\Html;
?>

<div class="profpopupholder">
	<div class="top">
		<span class="make-responsive">
            <?php if($model->profile->avatar):?>
                <div class="host-avatar-holder">
                	<?= Html::img('@web/images/avatar/'.$model->profile->avatar, ['class' => 'archive_host_image img-responsive']) ?> 
                	
                  </div>
            <?php else:?>
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>
            </span>
		<!-- <div class="pic">
			<?= Elements::image($model->profile->getImgSrc('avatar'), ['width' => '200px']) ?>
		</div> --><!--end pic-->
		<div class="details">
			<table class="detailstable">
				<tr>
					<td class="ceo-name"><?= $model->profile->name ?> <?= $model->profile->lastname ?></td>
				</tr>
				<tr>
					<td><?php echo $model->profile->designation ?></td>
				</tr>
				<tr>
					<td><?php echo $model->profile->organization ?></td>
				</tr>
				<!--
				<tr>
					<td>
						<?php # if($profile->twitter): ?>
						<a href="https://twitter.com/<?php # echo $profile -> twitter; ?>" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow CEo</a>
						<?php # endif; ?>
					</td>
				</tr>	-->											
			</table>
			
		</div> <!--end details-->
		<div class="clearfix"></div>
	</div><!--end top-->
	<div class="bio">
		<?php echo $model->profile->bio ?>
	</div><!--end end bio-->
</div>