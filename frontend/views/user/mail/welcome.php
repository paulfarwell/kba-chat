<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var dektrium\user\Module        $module
 * @var dektrium\user\models\User   $user
 * @var dektrium\user\models\Token  $token
 * @var bool                        $showPassword
 */

?>

<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0; color:#000">
    <?= Yii::t('user', 'Dear ') ?>,
    <strong><?= ucfirst($user->profile->name) . " " . ucfirst($user->profile->lastname)?></strong>
</p>

<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;color:#000">
    <?= Yii::t('user', 'Thank you for registering on {0} ', Yii::$app->name) ?>.
    <?php if ($showPassword || $module->enableGeneratingPassword): ?>
        <?= Yii::t('user', 'We have generated a password for you') ?>:
        <strong><?= $user->password ?></strong>
    <?php endif ?>

</p>

<?php if ($token !== null): ?>
<p style="font-family:'Raleway-Regular';font-size:15px;line-height:1.6;margin:0 0 10px;padding:0;font-weight:normal;padding-top:5px;color:#000">
    <?= Yii::t('user', 'In order to complete your registration , please click on the link below to authenticate. ') ?>.
</p>
<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;padding: 15px;">

<?= Html::a('Complete Registration',$token->url,['style'=>'color:#fff;text-decoration:none;background:#112d42; color:#fff; padding:10px; border:1px solid #112d42;padding:5px']);?>
</p>
<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;color:#000">
    <?= Yii::t('user', 'If you are unable to click on the link, you may copy and paste the link below into your browser.') ?>.

</p>
<p>  <?= Html::a(Html::encode($token->url), $token->url); ?></p>
<?php endif ?>
<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;color:#000">
    <?= Yii::t('user', 'If you did not make this request kindly can ignore this email') ?>.
</p>

<p style="font-family: 'Raleway-Regular'; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;color:#000">
  <?= Yii::t('user', ' Best Regards,') ?> <br/>
   <?= Yii::t('user', ' Gumzo Support Team.') ?>.
</p>
