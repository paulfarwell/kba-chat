<?php
/* @var $this yii\web\View */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
?>
<?php
$this->title = Yii::t('user', 'Gumzo');
$this->registerCssFile('/css/slick.css');
$this->registerCssFile('/css/slick-theme.css');
$this->registerJsFile('/js/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/chathelper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);



$js = <<<JS
 $('#events_section').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 8000,
  nextArrow: '<i class="arrow-right slick-next"></i>',
  prevArrow: '<i class="arrow-left slick-prev"></i>',
  responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true
        }
      },
    ]
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
$this->registerJs("
  var openigtime = moment(".strtotime($currentchat->OpenTime).");
  var chatStatus = ".$currentchat->Status.";
  var chatRoom = ".$currentchat->ID.";
  var mymemented=$('.memented');
  ", \yii\web\View::POS_END, 'current-chat');

AppAsset::register($this);
$detector = new \Mobile_Detect();
$bodyclass = "";

if ($detector->isMobile()) {
    $bodyclass = "mobile";
}

?>


<?php
$js2 = <<<JS
 $('#chat_summary_slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplay: true,
  autoplaySpeed: 8000,
  nextArrow: '<i class="fa fa-arrow-right slick-next"></i>',
  prevArrow: '<i class="fa fa-arrow-left slick-prev"></i>',
  dots: false,
  arrows:true,
  responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true,
        }
      },
    ]
});
JS;
$this->registerJs($js2, \yii\web\View::POS_READY);
?>
<?php
$jsspec = <<<JS
$('.spec_chat_details .media').matchHeight();
JS;
$this->registerJs($jsspec, \yii\web\View::POS_READY);
?>
<?= $this->render('@frontend/views/_confirmalert'); ?>
<?php if($bodyclass === "mobile"):?>
<?= $this->render('@frontend/themes/mobile/templates/mobile-home-jumbo',['chats'=>$chats]); ?>
<?php else:?>

<?= $this->render('@frontend/views/templates/home-jumbo',['chats'=>$chats, 'banners'=>$banners]); ?>

<?php endif;?>



<section class="chats_slider_title">
    <label class="text-center"> Chat with a Bank CEO Event Topics Includes: <hr></label>

</section>

<section class="chat-details-container container-grey">

    <div class="container"> <!-- main container -->

        <div class="row"> <!-- main-row -->

          <?= $this->render('@frontend/views/templates/event_topics');?>


        </div> <!-- main-row -->
    </div> <!-- main container -->
</section> <!-- container-section -->


<section class="chats_slider_title">
    <label class="text-center" style="color:#F78F1E;"> Chat Forums <?php echo date("Y");?> Edition <hr></label>

</section>

<section class="edition-details-container container-grey">

    <div class="container"> <!-- main container -->

        <div class="row"> <!-- main-row -->



            <?= $this->render('@frontend/views/chat/_chats_view',['editionchat'=>$editionchat]);?>
        </div> <!-- main-row -->

        <div class="row archives_index_btn">
          <div id="chat_archive_section">
            <div class="col-md-6 col-md-offset-6">
                <span class="pull-right">
                <?php echo Html::a('Chat Archives', ['/chat/archives'],['class'=>'btn btn-archive']); ?>
                </span>
            </div>
          </div>
        </div><!-- Chat Archive -->

    </div> <!-- main container -->
</section> <!-- container-section -->
