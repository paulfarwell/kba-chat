<?php
use backend\models\Profile;

?>
<?php foreach ($messages as $message):?>

	<?php $role = Profile::roleAlias($message->user->profile->role);?>
	<tr class="chat_message <?php echo strtolower($role);?>" id="message_<?php echo $message->ID; ?>">
		<td class="message_originator inner">
			<?php echo $message->ChatTime;?>
			<?php echo $message->role;?>
		</td>
		<?php if($message->role === 1):?>
		<td class="message_originator inner" style="color:#52c4ce;">

			<?php echo $message->UserName;?>
		</td>
	<?php elseif($message->role === 'user'):?>
     <td class="message_originator inner" style="color:#95C13D">
			<?php echo $message->UserName;?>
		</td>
	<?php else:?>
	<td class="message_originator inner" style="color:#002d3f">
			<?php echo $message->UserName;?>
		</td>
	<?php endif;?>	
		<td class="message_body">
			<?php echo $message->MessageText;?>
		</td>
	</tr>
<?php endforeach;?> 