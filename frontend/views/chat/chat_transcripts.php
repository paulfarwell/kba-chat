<?php 
use yii\helpers\Html;
?>
<div class="panel panel-default chat_transcript scrollable">

        <div class="panel-heading">CHAT TRANSCRIPTS 
         <?php if ($model->Log): ?>
            <?= Html::a('Download', Url::toRoute(['chat/dowload', 'id' => $model->ID]), ['title' => $model->Topic . "Details", 'class'=>"download_link btn btn-primary btn-xs"]) ?>
        <?php endif; ?>
        </div> <!-- panel-heading -->

        <div class="table-responsive" id="table_parent">

         <table class="table table-bordered table-striped table-hover" id="fixTable">
         <thead>
             <tr>
                 <th>Timestamp</th>
                 <th>Source</th>
                 <th>Message</th>
             </tr>
         </thead>
         <tbody>
             <?= $this->render('transcript', ['messages'=>$messages])?>
        </tbody>
        </table>

        </div><!-- table-responsive -->

    </div><!-- panel-default chat_transcript  -->

</div><!--  container -->