<?php
/**
 * var $model  chat model
 *
 */
 //print_r($model);
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container">
    <div class="row">
    	<div class="wizard">
            <div class="wizard-inner">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">

                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">

                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">

                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 4">

                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Step 5">

                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step6" data-toggle="tab" aria-controls="step6" role="tab" title="Step 6">

                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">

                        </a>
                    </li>
                </ul>
            </div>
            <form class="form-horizontal">

                  <div class="tab-content">
                     <?php foreach ($model->reviewQuestions as $value):?>
                        <input type="hidden" id="reviewQ" name="Reviews[QuestionsID]" value="<?php echo $value->ID;?>">
                        <input type="hidden" id="reviewUser" name="Reviews[UserID]" value="<?= \Yii::$app->user->identity->id ?>">
                        <input type="hidden" id="reviewChat" name="Reviews[Chat]" value="<?= $model->ID ?>">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="step1">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback1"><?php echo $value->Question_1;?></p>
                            <label class="radio"><input type="radio" value="1"  class="next-step" id="q1">Highly Recommend</label>
                            <label class="radio"><input type="radio" value="2"  class="next-step" id="q1">Recommend</label>
                            <label class="radio"><input type="radio" value="3"  class="next-step" id="q1">Average</label>
                            <label class="radio"><input type="radio" value="4"  class="next-step" id="q1">Below Average</label>
                            <label class="radio"><input type="radio" value="5"  class="next-step" id="q1">Bad</label>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback2"><?php echo $value->Question_2;?></p>
                            <label class="radio"><input type="radio" value="1" class="next-step" id="q2">Highly Recommend</label>
                            <label class="radio"><input type="radio" value="2" class="next-step" id="q2">Recommend</label>
                            <label class="radio"><input type="radio" value="3" class="next-step" id="q2">Average</label>
                            <label class="radio"><input type="radio" value="4" class="next-step" id="q2">Below Average</label>
                            <label class="radio"><input type="radio" value="5" class="next-step" id="q2">Bad</label>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step3">
                        <div class="step3">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback3"><?php echo $value->Question_3;?></p>
                            <label class="radio"><input type="radio" value="1" class="next-step" id="q3">Highly Recommend</label>
                            <label class="radio"><input type="radio" value="2" class="next-step" id="q3">Recommend</label>
                            <label class="radio"><input type="radio" value="3" class="next-step" id="q3">Average</label>
                            <label class="radio"><input type="radio" value="4" class="next-step" id="q3">Below Average</label>
                            <label class="radio"><input type="radio" value="5" class="next-step" id="q3">Bad</label>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step4">
                        <div class="step4">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback4"><?php echo $value->Question_4;?></p>
                            <label class="radio"><input type="radio" value="1" class="next-step" id="q4">Highly Recommend</label>
                            <label class="radio"><input type="radio" value="2" class="next-step" id="q4">Recommend</label>
                            <label class="radio"><input type="radio" value="3" class="next-step" id="q4">Average</label>
                            <label class="radio"><input type="radio" value="4" class="next-step" id="q4">Below Average</label>
                            <label class="radio"><input type="radio" value="5" class="next-step" id="q4">Bad</label>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step5">
                        <div class="step5">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback5"><?php echo $value->Question_5;?></p>
                            <label class="radio"><input type="radio" value="1" class="next-step" id="q5">Highly Recommend</label>
                            <label class="radio"><input type="radio" value="2" class="next-step" id="q5">Recommend</label>
                            <label class="radio"><input type="radio" value="3" class="next-step" id="q5">Average</label>
                            <label class="radio"><input type="radio" value="4" class="next-step" id="q5">Below Average</label>
                            <label class="radio"><input type="radio" value="5" class="next-step" id="q5">Bad</label>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step6">
                        <div class="step6">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback6">Please Leave a comment </p>
                             <textarea id="comment" class="form-control" rows="4" cols="50" style="height:auto"></textarea><br><br>
                            <input type="submit" class="btn btn-custom pull-right next-step" value="Send Feedback" id="q6">
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="complete">
                        <div class="complete">
                        <h2>Thank You for Giving Your Feedback on the Chat.</h2>
                        <br>
                        <a href="<?= Url::toRoute(['chat/index',])?>" class="btn btn-custom">Proceed To Home Page</a>
                    </div>
                    </div>
                <?php endforeach;?>
                </div>
                </form>


        </div>
   </div>
</div>
