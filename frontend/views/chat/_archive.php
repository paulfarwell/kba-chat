<?php
// _chats_view.php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Chat;
use Zelenin\yii\SemanticUI\Elements;
use frontend\models\User;
?>



<div class="archived_chat col-lg-4 col-md-4 col-sm-6 col-xs-12">
	<div class="archive_chat_holder">
		<?php if (User::findIdentity($model['Host'])->profile->avatar):?>
			 <div class = "image_holder">
				<!-- @web/images/avatar/'.User::findIdentity($model['Host'])->profile->avatar, -->
				<?= Html::img('@web/images/avatar/'.User::findIdentity($model['Host'])->profile->avatar, ['class' => 'img-thumbnail img-responsive','style'=>'width:220px;height:200px']) ?>
				<!-- archive_image -->
			</div>
		<?php else:?>
              <div class = "image_holder">
				<?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive','style'=>'width:220px;height:200px'])?>
			</div>
		<?php endif;?>

		<div class="topicinfo">
			<span style="display:inline-flex;"><p><b style="color:#000;">Title: </b></p>&nbsp; &nbsp;&nbsp; <p>  <?php echo $model['Topic']; ?></p></span><br>
			<span style="display:inline-flex;"><p><b style="color:#000;">Date :  </b></p>&nbsp;&nbsp;&nbsp; <p><?php echo date("jS F, Y",strtotime($model['ChatDate'])); ?></p></span><br>
			<span style="display:inline-flex;"><p><b style="color:#000;">HOST :  </b></p>&nbsp;&nbsp;&nbsp; <p><?php echo User::findIdentity($model['Host'])->profile->name." ".User::findIdentity($model['Host'])->profile->lastname. " ".User::findIdentity($model['Host'])->profile->organization; ?></p>
		</div>
		<div class="details_sect">
			<?php if ($model['Podcast'] != NULL):?>
				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Podcast', Url::toRoute(['chat/view', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-chatroom '] ) ?>
			<?php endif;?>

				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Transcript', Url::toRoute(['chat/view', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-chatroom ']) ?>

				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Details', Url::toRoute(['chat/details', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-chatroom ']) ?>
		</div>

	</div><hr><!-- 	archive_chat_holder -->
</div><!-- MAIN col-xs-12 col-md-6 -->
