<?php
// _chats_view.php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Chat;
use Zelenin\yii\SemanticUI\Elements;
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
<?php foreach( $editionchat as $chat):?>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	<div class="row this-chat chat_details  border-right">
		<div class="media">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if(empty($chat->host->profile->avatar)):?>
					<div class="home_image_holder">
										<?= Html::img('@web/images/avatar/user.jpg',['class' => ' img-responsive'])?>
										</div>
					<?php else:?>
				<div class="home_image_holder">
					<!-- @web/images/avatar/'.$chat->host->profile->avatar -->
				<?= Html::img('@web/images/avatar/'.$chat->host->profile->avatar,['class' => 'edition-image  img-responsive'])?>
												<div class="edition-middle">
									 <div class="edition-text">
										 <div class="media-body">
											 <div class="row" id="date">
												 <div class="col-lg-3 col-xs-3 medium-bold">
													 <?= Html::encode("DATE: "); ?>
												 </div>
												 <div class="col-lg-9 col-xs-9">
													 <?= Html::encode($chat->ChatDate); ?>
												 </div>
											 </div>
											 <div class="row" id="time">
												 <div class="col-lg-3 col-xs-3 medium-bold">
													 <?= Html::encode("TIME: "); ?>
												 </div>
												 <div class="col-lg-9 col-xs-9">
													 <?= Html::encode(Yii::$app->formatter->asTime($chat->StartTime, "HH:mm a")." - ".Yii::$app->formatter->asTime($chat->EndTime, "HH:mm a")); ?>
												 </div>
											 </div>
											 <div class="row" id="host">
												 <div class="col-lg-3 col-xs-3 medium-bold">
													 <?= Html::encode("HOST: "); ?>
												 </div>
												 <div class="col-lg-9 col-xs-9">
													 <?= Html::encode($chat->ceostring); ?>
												 </div>
											 </div>
											 <div class="row" id="topic">
												 <div class="col-lg-3 col-xs-3 medium-bold">
													 <?= Html::encode("TOPIC: "); ?>
												 </div>
												 <div class="col-lg-9 col-xs-9">
													 <?= Html::encode($chat->Topic); ?>
												 </div>
											 </div>
										 </div>
										 <div class="row actions">

											 <div class="media-body">
															<ul class="chat-view-ul" style="margin-left:-26px;">
															 <li>

													 <?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]).'Details .',Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->Topic . "Details"]) ?>
															 </li>
															 <li>
																 <?php if ($chat->Podcast != NULL):?>
													 <?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]). 'Listen .', Url::toRoute(['chat/view', 'id' => $chat->ID]), ['title' => $chat->Topic . "Watch"]) ?>
												 <?php else:?>
													 <?php echo Html::tag('span', '', [ 'aria-hidden' => 'true',]). " Listen ."; ?>
												 <?php endif;?>
															 </li>
															 <li>
																 <?php if ($chat->Log != NULL):?>
													 <?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]).'Read ', Url::toRoute(['chat/view', 'id' => $chat->ID]), ['title' => $chat->Topic . " Read"]) ?>
												 <?php else:?>
													 <?php echo Html::tag('span', '', [ 'aria-hidden' => 'true',]). " Read"; ?>
												 <?php endif;?>
												 </span>
															 </li>

															</ul>
															</div>
													
												<!-- col-xs-3 -->
														<!-- col-xs-3 -->
										 </div>
									 </div>
								 </div>
						</div>
		<?php endif;?>
			</div>
		</div>
	</div>
</div>
<?php endforeach;?>
</div>
