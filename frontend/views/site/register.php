<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;
use common\models\Industry;
use common\models\Profession;
use common\models\County;

/**
 * @var yii\web\View              $this
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

$this->title = Yii::t('user', 'Register & Login');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/farwell.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$settings = Yii::$app->settings;
$settings->clearCache();
$reginfo = $settings->get('Theme.registrationDescription');
?>

<!-- Header -->
<?= $this->render('@frontend/views/templates/metro_register') ?>

<section class="chat-details-container container-grey">
    <div class="container section-register">

        <div class="row">

            <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6 col-md-offset-3 col-lg-offset-3" id="register">



                    <div class="registration-form form-white">

                        <h1 >Register <span style="color:#000">& Join My Chat with a Bank CEO</span> </h1>

                        <p style="text-align:center"> “My Chat with a Bank CEO” was created by the Kenya Bankers Association in July 2012
                            to provide a platform for bank Chief Executive Officers to engage with the public on
                            various topics on banking.

                            Hundreds of people like you have registered to participate in the quarterly live Web
                            chat sessions during which the CEOs share information, give advice and receive feedback.

                            The sessions take place once a quarter, over a period of four weeks. Each session lasts
                            approximately one hour.</p>

                                <?php $form = ActiveForm::begin([
                                    'id' => 'registration-form',
                                    'class' => 'form-horizontal form-white',
                                    'layout' => 'horizontal',
                                    'fieldConfig' => [
                                        'horizontalCssClasses' => [
                                            'label' => 'col-xs-12 col-sm-3',
                                            'offset' => '',
                                            'wrapper' => 'col-xs-12 col-sm-12 col-md-12 col-lg-10',
                                            'error' => '',
                                            'hint' => '',
                                        ],
                                        'horizontalCheckboxTemplate'=>"<div class=\"form-group\">\n<div class=\"checkbox\">\n<div class=\"col-sm-1 col-xs-3\">\n{input}\n</div>\n<label class=\"col-sm-8 col-xs-8 col-md-8 col-lg-8 control-label\">\n{labelTitle}\n{error}\n</label>\n</div>\n</div>\n{hint}",

                                    ],
                                ]); ?>

                                <?= $form->field($model, 'name')->input('name',['placeholder'=>'First Name'])->label(false) ?>

                                <?= $form->field($model, 'lastname')->input('lastname',['placeholder'=>'Last Name'])->label(false) ?>

                                <?= $form->field($model, 'email')->input('email',['placeholder'=>'Email'])->label(false) ?>

                               <!-- <?= $form->field($model, 'username') ?>-->

                                <?= $form->field($model, 'password')->passwordInput()->input('password',['placeholder'=>'Password'])->label(false) ?>

                                <?= $form->field($model, 'confirm_password')->passwordInput()->input('password',['placeholder'=>'Confirm Password'])->label(false) ?>
                                <?= $form->field($model, 'industry')
                                        ->dropDownList(
                                            ArrayHelper::map(Industry::find()->orderBy('name')->all(), 'id', 'name'),
                                            ['prompt'=>'Select Industry']
                                        )->label(false);
                                 ?>

                                 <?= $form->field($model, 'profession')
                                         ->dropDownList(
                                             ArrayHelper::map(Profession::find()->orderBy('name')->all(), 'id', 'name'),
                                             ['prompt'=>'Select Profession']
                                         )->label(false);
                                  ?>
                                <?= $form->field($model, 'country')
                                        ->dropDownList(
                                            ArrayHelper::map(Countries::find()->orderBy('CountryName')->all(), 'ID', 'CountryName'),
                                            ['prompt'=>'Select Country']
                                        )->label(false);
                                 ?>
                                 <?= $form->field($model, 'county')
                                         ->dropDownList(
                                             ArrayHelper::map(County::find()->orderBy('name')->all(), 'id', 'name'),
                                             ['prompt'=>'Select County']
                                         )->label(false);
                                  ?>
                                <?= $form->field($model, 'city')->input('city',['placeholder'=>'City'])->label(false) ?>

                                 <?= $form->field($model, 'termsagree')->checkbox(); ?>


                                <p class="email_note">* The system will send an email. Please click on the link to activate your account</p>

                                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-archive sign-up']) ?>

                                <?php ActiveForm::end(); ?>

                    </div><!-- registration-form -->

            </div><!-- col-md-6 -->

         </div><!-- row -->


 </div> <!-- main container -->
</section> <!-- container-section -->

<?= $this->render('login', ['model'=>$loginmodel,'module'=>$module])?>
