<?php
// use app\modules\demo\widgets\Example;
use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */

$this->title = 'Reviews and Feedback';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@frontend/views/templates/review_menu')?>
 
<div class="row user_profile">


    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [ 
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <div class="col-md-7 col-lg-7 col-xs-12 col-sm-12 col-lg-offset-2 col-md-offset-3">
            <div class="profile_edit ">
                
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['site/storereview'],
                        'options' => ['method' => 'post','class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'],
                        
                      
                    ]); ?>
                    
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
                          <input type="hidden" name="Reviews[QuestionsID]" value="<?php echo $question->ID;?>">
                        <input type="hidden" name="Reviews[UserID]" value="<?= \Yii::$app->user->identity->id ?>">
                        <?php echo $question->Question_1;?>
                        <?= $form->field($model, 'Answer_1')->textarea()->label(false) ?>
                        
                         <?php echo $question->Question_2;?>
                        <?= $form->field($model, 'Answer_2')->textarea()->label(false) ?>
                        
                        <?php if(empty($question->Question_3)):?>


                        <?php else:?>
                         <?php echo $question->Question_3;?>
                        <?= $form->field($model, 'Answer_3')->textarea()->label(false) ?>
                          <?php endif;?>
                          <?php if(empty($question->Question_4)):?>


                        <?php else:?>
                         <?php echo $question->Question_4;?>
                        <?= $form->field($model, 'Answer_4')->textarea()->label(false) ?>
                          <?php endif;?>
                         
                         <?php if(empty($question->Question_5)):?>


                        <?php else:?>
                         <?php echo $question->Question_5;?>
                        <?= $form->field($model, 'Answer_5')->textarea()->label(false) ?>
                          <?php endif;?>

                        <div class="form-group">
                            <div class="col-lg-4 pull-right">
                                <?= \yii\helpers\Html::submitButton(Yii::t('user', 'Send Review'), ['class' => 'btn btn-block btn-join', 'style'=>'text-transform:initial']) ?><br>
                            </div>
                        </div>

                    </div>
                        <?php \yii\widgets\ActiveForm::end(); ?>
                
            </div>
        </div>

   </div><!--  container --> 
</div><!-- user_profile -->
