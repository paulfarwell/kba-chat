<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use Zelenin\yii\SemanticUI\modules\Modal;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$settings = Yii::$app->settings;
// $settings->clearCache();
$logininfo = $settings->get('Theme.loginDescription');

?>

<?php

    Modal::begin([
            'header' => '<h4>Destination</h4>',
            'id'     => 'model',
            'size'   => 'model-lg',
    ]);?>

    <section class=" chat-details-container container-orange">

        <div class="container section-login">

            <div class="row">

                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">

                    <div class="login-form form-black" id="login_section">
                        <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

                        <?php $form = ActiveForm::begin([
                            'id'                     => 'request-demo',
                            'enableAjaxValidation'   => true,
                            'enableClientValidation' => false,
                            'validateOnBlur'         => false,
                            'validateOnType'         => false,
                            'validateOnChange'       => false,
                            'class' => 'form-horizontal',
                            'layout' => 'horizontal',
                            'fieldConfig' => [
                                'horizontalCssClasses' => [
                                    'label' => 'col-xs-12 col-sm-3',
                                    'offset' => '',
                                    'wrapper' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
                                    'error' => '',
                                    'hint' => '',
                                ],
                            ],
                        ]) ?>

                        <?= $form->field($model, 'name', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '1']])->label(false)->input('name',['placeholder'=>'Full Name']) ?>

                         <div class="clearfix"> </div>

                        <div class="col-tight-left col-sm-6">
                            <div class="pull-left">

                        <?= Html::submitButton(Yii::t('user', 'join Chatroom'), ['class' => 'btn btn-join join-chat', 'tabindex' => '3']) ?>
                           </div>
                        </div>

                        <div class="col-sm-6">

                        </div>
                        <div class="clearfix"> </div>

                        <?php ActiveForm::end(); ?>

                    </div> <!-- login_section -->


                 </div><!-- col-md-6 -->


            </div><!-- main row -->

        </div> <!-- main container -->
    </section> <!-- container-section -->;

  <?php  Modal::end();

?>
