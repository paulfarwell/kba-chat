<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$settings = Yii::$app->settings;
// $settings->clearCache();
$logininfo = $settings->get('Theme.loginDescription');

?>

<section class=" chat-details-container container-orange">

    <div class="container section-login">

        <div class="row">

            <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6 col-md-offset-3 col-lg-offset-3" id="login">

                <div class="login-form form-black" id="login_section" style="padding-top: 20px">
                    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

                    <h1 style="padding-left:20px;color:#ffffff">Already registered? login & join Chatroom</h1>
                    <?php $form = ActiveForm::begin([
                        'id'                     => 'login-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'validateOnBlur'         => false,
                        'validateOnType'         => false,
                        'validateOnChange'       => false,
                        'class' => 'form-horizontal',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'label' => 'col-xs-12 col-sm-3',
                                'offset' => '',
                                'wrapper' => 'col-xs-12 col-sm-12 col-md-12 col-lg-10',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
                    ]) ?>

                    <?= $form->field($model, 'login', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '1']])->label(false)->input('login',['placeholder'=>'Email Address']) ?>

                    <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(false)->input('password',['placeholder'=>'Password']) ?>

                     <div class="clearfix"> </div>

                    <div class="col-tight-left col-sm-6">
                        <div class="pull-left">

                    <?= Html::submitButton(Yii::t('user', 'join Chatroom'), ['class' => 'btn btn-chatroom join-chat', 'tabindex' => '3']) ?>


                       </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="pull-right">
                          <?=  ($module->enablePasswordRecovery ? Html::a(Yii::t('user', 'Reset password'), ['/user/recovery/request'],['class' => 'btn btn-archive', 'tabindex' => '3']) : '')?>
                        </div>
                    </div>
                    <div class="clearfix"> </div>

                    <?php ActiveForm::end(); ?>

                </div> <!-- login_section -->


             </div><!-- col-md-6 -->


        </div><!-- main row -->

    </div> <!-- main container -->
</section> <!-- container-section -->
