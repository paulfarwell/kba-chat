<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbdc6d0c7cd044c065e20dfe30011c173
{
    public static $files = array (
        '2cffec82183ee1cea088009cef9a6fc3' => __DIR__ . '/..' . '/ezyang/htmlpurifier/library/HTMLPurifier.composer.php',
        '2c102faa651ef8ea5874edb585946bce' => __DIR__ . '/..' . '/swiftmailer/swiftmailer/lib/swift_required.php',
    );

    public static $prefixLengthsPsr4 = array (
        'y' => 
        array (
            'yii\\swiftmailer\\' => 16,
            'yii\\redis\\' => 10,
            'yii\\jui\\' => 8,
            'yii\\imagine\\' => 12,
            'yii\\gii\\' => 8,
            'yii\\faker\\' => 10,
            'yii\\debug\\' => 10,
            'yii\\composer\\' => 13,
            'yii\\codeception\\' => 16,
            'yii\\bootstrap\\' => 14,
            'yii\\authclient\\' => 15,
            'yii\\' => 4,
        ),
        's' => 
        array (
            'sjaakp\\illustrated\\' => 19,
        ),
        'r' => 
        array (
            'rmrevin\\yii\\fontawesome\\' => 24,
        ),
        'p' => 
        array (
            'pheme\\settings\\' => 15,
        ),
        'k' => 
        array (
            'kop\\y2sp\\' => 9,
            'kartik\\time\\' => 12,
            'kartik\\select2\\' => 15,
            'kartik\\plugins\\fileinput\\' => 25,
            'kartik\\file\\' => 12,
            'kartik\\base\\' => 12,
        ),
        'd' => 
        array (
            'dosamigos\\datetimepicker\\' => 25,
            'dosamigos\\datepicker\\' => 21,
            'dosamigos\\ckeditor\\' => 19,
            'dektrium\\user\\' => 14,
            'dektrium\\rbac\\' => 14,
        ),
        'c' => 
        array (
            'cebe\\markdown\\' => 14,
        ),
        'a' => 
        array (
            'alexandernst\\devicedetect\\' => 26,
        ),
        '\\' => 
        array (
            '\\pheme\\grid\\' => 12,
        ),
        'Z' => 
        array (
            'Zelenin\\yii\\SemanticUI\\' => 23,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'yii\\swiftmailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-swiftmailer',
        ),
        'yii\\redis\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-redis',
        ),
        'yii\\jui\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-jui',
        ),
        'yii\\imagine\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-imagine',
        ),
        'yii\\gii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-gii',
        ),
        'yii\\faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-faker',
        ),
        'yii\\debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-debug',
        ),
        'yii\\composer\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-composer',
        ),
        'yii\\codeception\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-codeception',
        ),
        'yii\\bootstrap\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-bootstrap',
        ),
        'yii\\authclient\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2-authclient',
        ),
        'yii\\' => 
        array (
            0 => __DIR__ . '/..' . '/yiisoft/yii2',
        ),
        'sjaakp\\illustrated\\' => 
        array (
            0 => __DIR__ . '/..' . '/sjaakp/yii2-illustrated-behavior',
        ),
        'rmrevin\\yii\\fontawesome\\' => 
        array (
            0 => __DIR__ . '/..' . '/rmrevin/yii2-fontawesome',
        ),
        'pheme\\settings\\' => 
        array (
            0 => __DIR__ . '/..' . '/pheme/yii2-settings',
        ),
        'kop\\y2sp\\' => 
        array (
            0 => __DIR__ . '/..' . '/kop/yii2-scroll-pager',
        ),
        'kartik\\time\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-widget-timepicker',
        ),
        'kartik\\select2\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-widget-select2',
        ),
        'kartik\\plugins\\fileinput\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/bootstrap-fileinput',
        ),
        'kartik\\file\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-widget-fileinput',
        ),
        'kartik\\base\\' => 
        array (
            0 => __DIR__ . '/..' . '/kartik-v/yii2-krajee-base',
        ),
        'dosamigos\\datetimepicker\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-date-time-picker-widget/src',
        ),
        'dosamigos\\datepicker\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-date-picker-widget/src',
        ),
        'dosamigos\\ckeditor\\' => 
        array (
            0 => __DIR__ . '/..' . '/2amigos/yii2-ckeditor-widget/src',
        ),
        'dektrium\\user\\' => 
        array (
            0 => __DIR__ . '/..' . '/dektrium/yii2-user',
        ),
        'dektrium\\rbac\\' => 
        array (
            0 => __DIR__ . '/..' . '/dektrium/yii2-rbac',
        ),
        'cebe\\markdown\\' => 
        array (
            0 => __DIR__ . '/..' . '/cebe/markdown',
        ),
        'alexandernst\\devicedetect\\' => 
        array (
            0 => __DIR__ . '/..' . '/alexandernst/yii2-device-detect',
        ),
        '\\pheme\\grid\\' => 
        array (
            0 => __DIR__ . '/..' . '/pheme/yii2-toggle-column',
        ),
        'Zelenin\\yii\\SemanticUI\\' => 
        array (
            0 => __DIR__ . '/..' . '/zelenin/yii2-semantic-ui',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
    );

    public static $prefixesPsr0 = array (
        'I' => 
        array (
            'Imagine' => 
            array (
                0 => __DIR__ . '/..' . '/imagine/imagine/lib',
            ),
        ),
        'H' => 
        array (
            'HTMLPurifier' => 
            array (
                0 => __DIR__ . '/..' . '/ezyang/htmlpurifier/library',
            ),
        ),
        'D' => 
        array (
            'Diff' => 
            array (
                0 => __DIR__ . '/..' . '/phpspec/php-diff/lib',
            ),
            'Detection' => 
            array (
                0 => __DIR__ . '/..' . '/mobiledetect/mobiledetectlib/namespaced',
            ),
        ),
    );

    public static $classMap = array (
        'Mobile_Detect' => __DIR__ . '/..' . '/mobiledetect/mobiledetectlib/Mobile_Detect.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbdc6d0c7cd044c065e20dfe30011c173::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbdc6d0c7cd044c065e20dfe30011c173::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitbdc6d0c7cd044c065e20dfe30011c173::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitbdc6d0c7cd044c065e20dfe30011c173::$classMap;

        }, null, ClassLoader::class);
    }
}
