<?php 

namespace common\widgets;


use yii\base\Widget;
use yii\helpers\Html;

class Banner extends Widget
{
    public $message;

    public $banner_id ='bannerWidget_id';

    public $options = [];

    public $bannerProperties= [];

    private $bannerItemsContent;

    public function init()
    {
        parent::init();
        $this->bannerItemsContent = [];
        $this->bannerItemsContent = $this->bannerProperties;


    }

    public function run()
    {
        return $this->render('banner', ['bannerItemsContent' => $this->bannerItemsContent]);
    }
}