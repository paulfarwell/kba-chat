<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "participate".
 *
 * @property integer $ID
 * @property integer $User
 * @property integer $Chat
 * @property integer $Termsagree
 * @property string $RequestDate
 * @property integer $Status
 */
class Participate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participate';
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['RequestDate'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User', 'Chat'], 'required'],
            [['User', 'Chat', 'Termsagree', 'Status'], 'integer'],
            [['RequestDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'User' => 'User',
            'Chat' => 'Chat',
            'Termsagree' => 'Termsagree',
            'RequestDate' => 'Request Date',
            'Status' => 'Status',
        ];
    }
}
