<?php
namespace common\models;

use Yii;
use common\models\Chat;
use backend\models\ReviewQuestions;

class DemoRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'demo_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['name','email','phone_number','organization','request'], 'required'],
           [['name', 'email','phone_number','organization','request'], 'string', 'max' => 255],
           ['email','email']


        ];
    }
    public function attributeLabels()
    {
        return [
           'id' => 'ID',
           'name' => 'Full Name',
           'email' => 'Email',
           'phone_number' => 'Phone Number',
           'organization' => 'Organization',
           'request' => 'Request Message',
        ];
    }


}
