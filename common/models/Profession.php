<?php
namespace common\models;

use Yii;


class Profession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'professions';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
          'id' => 'ID',
           'name' => 'Profession',

        ];
    }
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return [
    //        [['Chat','AnswersID','Question_1', 'Question_2'], 'required'],
    //         [['Question_1', 'Question_2','Question_3','Question_4','Question_5'], 'string', 'max' => 255]


    //     ];
    // }



}
